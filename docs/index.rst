Welcome to negotiate's documentation!
======================================

.. toctree::
   :maxdepth: 2

   readme
   installation
   overview
   tutorials
   base_modules
   application_modules
   helper_modules
   api
   contributing
   authors
   history

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
