Helper Modules
===============

.. automodapi:: negotiate.common
      :members:
      :show-inheritance:
.. automodapi:: negotiate.generics
      :members:
      :show-inheritance:
.. automodapi:: negotiate.helpers
      :members:
      :show-inheritance:
.. automodapi:: negotiate.inout
      :members:
      :show-inheritance:

