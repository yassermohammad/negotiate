Base Modules
============

.. automodapi:: negotiate.outcomes
      :members:
      :show-inheritance:
.. automodapi:: negotiate.utilities
      :members:
      :show-inheritance:
.. automodapi:: negotiate.negotiators
      :members:
      :show-inheritance:
.. automodapi:: negotiate.mechanisms
      :members:
      :show-inheritance:
.. automodapi:: negotiate.genius
      :members:
      :show-inheritance:
.. automodapi:: negotiate.situated
      :members:
      :show-inheritance:
