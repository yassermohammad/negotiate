Reference
=========
.. automodapi:: negotiate.situated    
.. automodapi:: negotiate.sao    
.. automodapi:: negotiate.genius    
.. automodapi:: negotiate.events    
.. automodapi:: negotiate.negotiators    
.. automodapi:: negotiate.common    
.. automodapi:: negotiate.outcomes    
.. automodapi:: negotiate.acceptance_models    
.. automodapi:: negotiate.utilities    
.. automodapi:: negotiate.mechanisms    
.. automodapi:: negotiate.helpers    
.. automodapi:: negotiate.inout    
.. automodapi:: negotiate.generics    
    :no-heading:    
