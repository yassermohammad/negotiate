ScheduleInfo
============

.. currentmodule:: negotiate.apps.scml

.. autoclass:: ScheduleInfo
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~ScheduleInfo.end
      ~ScheduleInfo.valid

   .. rubric:: Methods Summary

   .. autosummary::

      ~ScheduleInfo.combine

   .. rubric:: Attributes Documentation

   .. autoattribute:: end
   .. autoattribute:: valid

   .. rubric:: Methods Documentation

   .. automethod:: combine
