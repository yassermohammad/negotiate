outcome_in_range
================

.. currentmodule:: negotiate.outcomes

.. autofunction:: outcome_in_range
