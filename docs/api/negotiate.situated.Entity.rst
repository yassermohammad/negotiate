Entity
======

.. currentmodule:: negotiate.situated

.. autoclass:: Entity
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~Entity.init

   .. rubric:: Methods Documentation

   .. automethod:: init
