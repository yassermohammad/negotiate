ComplexWeightedUtilityFunction
==============================

.. currentmodule:: negotiate.utilities

.. autoclass:: ComplexWeightedUtilityFunction
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~ComplexWeightedUtilityFunction.__call__
      ~ComplexWeightedUtilityFunction.xml

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: xml
