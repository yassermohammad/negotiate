LimitedOutcomesNegotiator
=========================

.. currentmodule:: negotiate.sao

.. autoclass:: LimitedOutcomesNegotiator
   :show-inheritance:
