Scheduler
=========

.. currentmodule:: negotiate.apps.scml

.. autoclass:: Scheduler
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~Scheduler.find_schedule
      ~Scheduler.init
      ~Scheduler.reset_schedule
      ~Scheduler.schedule

   .. rubric:: Methods Documentation

   .. automethod:: find_schedule
   .. automethod:: init
   .. automethod:: reset_schedule
   .. automethod:: schedule
