outcome_range_is_valid
======================

.. currentmodule:: negotiate.outcomes

.. autofunction:: outcome_range_is_valid
