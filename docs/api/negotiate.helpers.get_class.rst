get_class
=========

.. currentmodule:: negotiate.helpers

.. autofunction:: get_class
