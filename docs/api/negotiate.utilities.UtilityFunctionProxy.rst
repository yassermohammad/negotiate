UtilityFunctionProxy
====================

.. currentmodule:: negotiate.utilities

.. autoclass:: UtilityFunctionProxy
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~UtilityFunctionProxy.base_type
      ~UtilityFunctionProxy.is_dynamic
      ~UtilityFunctionProxy.type

   .. rubric:: Methods Summary

   .. autosummary::

      ~UtilityFunctionProxy.__call__
      ~UtilityFunctionProxy.approximate
      ~UtilityFunctionProxy.compare
      ~UtilityFunctionProxy.conflict_level
      ~UtilityFunctionProxy.eu
      ~UtilityFunctionProxy.from_genius
      ~UtilityFunctionProxy.from_xml_str
      ~UtilityFunctionProxy.generate_bilateral
      ~UtilityFunctionProxy.generate_random_bilateral
      ~UtilityFunctionProxy.to_genius
      ~UtilityFunctionProxy.to_xml_str
      ~UtilityFunctionProxy.winwin_level
      ~UtilityFunctionProxy.xml

   .. rubric:: Attributes Documentation

   .. autoattribute:: base_type
   .. autoattribute:: is_dynamic
   .. autoattribute:: type

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: approximate
   .. automethod:: compare
   .. automethod:: conflict_level
   .. automethod:: eu
   .. automethod:: from_genius
   .. automethod:: from_xml_str
   .. automethod:: generate_bilateral
   .. automethod:: generate_random_bilateral
   .. automethod:: to_genius
   .. automethod:: to_xml_str
   .. automethod:: winwin_level
   .. automethod:: xml
