LoggerMixin
===========

.. currentmodule:: negotiate.helpers

.. autoclass:: LoggerMixin
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~LoggerMixin.logdebug
      ~LoggerMixin.logerror
      ~LoggerMixin.loginfo
      ~LoggerMixin.logwarning

   .. rubric:: Methods Documentation

   .. automethod:: logdebug
   .. automethod:: logerror
   .. automethod:: loginfo
   .. automethod:: logwarning
