UncertainOpponentModel
======================

.. currentmodule:: negotiate.acceptance_models

.. autoclass:: UncertainOpponentModel
   :show-inheritance:
