World
=====

.. currentmodule:: negotiate.situated

.. autoclass:: World
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~World.relative_time
      ~World.remaining_steps
      ~World.remaining_time
      ~World.stats
      ~World.time

   .. rubric:: Methods Summary

   .. autosummary::

      ~World.execute
      ~World.join
      ~World.logdebug
      ~World.logerror
      ~World.loginfo
      ~World.logwarning
      ~World.on_contract_concluded
      ~World.on_contract_signed
      ~World.register
      ~World.request_negotiation
      ~World.run
      ~World.run_negotiation
      ~World.set_bulletin_board
      ~World.state
      ~World.step

   .. rubric:: Attributes Documentation

   .. autoattribute:: relative_time
   .. autoattribute:: remaining_steps
   .. autoattribute:: remaining_time
   .. autoattribute:: stats
   .. autoattribute:: time

   .. rubric:: Methods Documentation

   .. automethod:: execute
   .. automethod:: join
   .. automethod:: logdebug
   .. automethod:: logerror
   .. automethod:: loginfo
   .. automethod:: logwarning
   .. automethod:: on_contract_concluded
   .. automethod:: on_contract_signed
   .. automethod:: register
   .. automethod:: request_negotiation
   .. automethod:: run
   .. automethod:: run_negotiation
   .. automethod:: set_bulletin_board
   .. automethod:: state
   .. automethod:: step
