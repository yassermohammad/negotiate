LineState
=========

.. currentmodule:: negotiate.apps.scml

.. autoclass:: LineState
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~LineState.commands
      ~LineState.current_process
      ~LineState.jobs
      ~LineState.paused_at
      ~LineState.running_for
      ~LineState.schedule
      ~LineState.updates

   .. rubric:: Attributes Documentation

   .. autoattribute:: commands
   .. autoattribute:: current_process
   .. autoattribute:: jobs
   .. autoattribute:: paused_at
   .. autoattribute:: running_for
   .. autoattribute:: schedule
   .. autoattribute:: updates
