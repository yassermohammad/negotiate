PeekingAcceptanceModel
======================

.. currentmodule:: negotiate.acceptance_models

.. autoclass:: PeekingAcceptanceModel
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~PeekingAcceptanceModel.probability_of_acceptance_indx
      ~PeekingAcceptanceModel.update_offered_indx
      ~PeekingAcceptanceModel.update_rejected_indx

   .. rubric:: Methods Documentation

   .. automethod:: probability_of_acceptance_indx
   .. automethod:: update_offered_indx
   .. automethod:: update_rejected_indx
