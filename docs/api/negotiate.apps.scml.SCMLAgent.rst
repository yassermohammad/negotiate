SCMLAgent
=========

.. currentmodule:: negotiate.apps.scml

.. autoclass:: SCMLAgent
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~SCMLAgent.factory_state

   .. rubric:: Methods Summary

   .. autosummary::

      ~SCMLAgent.attach
      ~SCMLAgent.before_joining_negotiation
      ~SCMLAgent.can_expect_agreement
      ~SCMLAgent.confirm_contract_execution
      ~SCMLAgent.confirm_loan
      ~SCMLAgent.init
      ~SCMLAgent.on_breach_by_another
      ~SCMLAgent.on_breach_by_self
      ~SCMLAgent.on_breach_meta_negotiation
      ~SCMLAgent.on_negotiation_request
      ~SCMLAgent.on_new_cfp
      ~SCMLAgent.on_production_failure
      ~SCMLAgent.on_renegotiation_request
      ~SCMLAgent.step

   .. rubric:: Attributes Documentation

   .. autoattribute:: factory_state

   .. rubric:: Methods Documentation

   .. automethod:: attach
   .. automethod:: before_joining_negotiation
   .. automethod:: can_expect_agreement
   .. automethod:: confirm_contract_execution
   .. automethod:: confirm_loan
   .. automethod:: init
   .. automethod:: on_breach_by_another
   .. automethod:: on_breach_by_self
   .. automethod:: on_breach_meta_negotiation
   .. automethod:: on_negotiation_request
   .. automethod:: on_new_cfp
   .. automethod:: on_production_failure
   .. automethod:: on_renegotiation_request
   .. automethod:: step
