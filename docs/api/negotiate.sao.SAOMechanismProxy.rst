SAOMechanismProxy
=================

.. currentmodule:: negotiate.sao

.. autoclass:: SAOMechanismProxy
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~SAOMechanismProxy.extra_state
      ~SAOMechanismProxy.on_negotiation_start
      ~SAOMechanismProxy.round

   .. rubric:: Methods Documentation

   .. automethod:: extra_state
   .. automethod:: on_negotiation_start
   .. automethod:: round
