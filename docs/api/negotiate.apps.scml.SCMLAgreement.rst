SCMLAgreement
=============

.. currentmodule:: negotiate.apps.scml

.. autoclass:: SCMLAgreement
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~SCMLAgreement.penalty
      ~SCMLAgreement.signing_delay

   .. rubric:: Attributes Documentation

   .. autoattribute:: penalty
   .. autoattribute:: signing_delay
