AspirationNegotiator
====================

.. currentmodule:: negotiate.sao

.. autoclass:: AspirationNegotiator
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~AspirationNegotiator.eu

   .. rubric:: Methods Summary

   .. autosummary::

      ~AspirationNegotiator.on_negotiation_start
      ~AspirationNegotiator.on_notification
      ~AspirationNegotiator.propose_
      ~AspirationNegotiator.respond_

   .. rubric:: Attributes Documentation

   .. autoattribute:: eu

   .. rubric:: Methods Documentation

   .. automethod:: on_negotiation_start
   .. automethod:: on_notification
   .. automethod:: propose_
   .. automethod:: respond_
