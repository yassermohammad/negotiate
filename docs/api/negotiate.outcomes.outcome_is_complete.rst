outcome_is_complete
===================

.. currentmodule:: negotiate.outcomes

.. autofunction:: outcome_is_complete
