ComplexNonlinearUtilityFunction
===============================

.. currentmodule:: negotiate.utilities

.. autoclass:: ComplexNonlinearUtilityFunction
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~ComplexNonlinearUtilityFunction.__call__
      ~ComplexNonlinearUtilityFunction.xml

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: xml
