Issues
======

.. currentmodule:: negotiate.outcomes

.. autoclass:: Issues
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Issues.all
      ~Issues.outcome_range
      ~Issues.types

   .. rubric:: Methods Summary

   .. autosummary::

      ~Issues.cardinality
      ~Issues.from_issue_collection
      ~Issues.from_single_issue
      ~Issues.is_finite
      ~Issues.is_infinite
      ~Issues.n_outcomes
      ~Issues.rand
      ~Issues.rand_invalid
      ~Issues.rand_valid

   .. rubric:: Attributes Documentation

   .. autoattribute:: all
   .. autoattribute:: outcome_range
   .. autoattribute:: types

   .. rubric:: Methods Documentation

   .. automethod:: cardinality
   .. automethod:: from_issue_collection
   .. automethod:: from_single_issue
   .. automethod:: is_finite
   .. automethod:: is_infinite
   .. automethod:: n_outcomes
   .. automethod:: rand
   .. automethod:: rand_invalid
   .. automethod:: rand_valid
