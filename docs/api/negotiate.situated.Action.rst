Action
======

.. currentmodule:: negotiate.situated

.. autoclass:: Action
   :show-inheritance:
