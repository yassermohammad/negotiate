AggregatingAcceptanceModel
==========================

.. currentmodule:: negotiate.acceptance_models

.. autoclass:: AggregatingAcceptanceModel
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~AggregatingAcceptanceModel.probability_of_acceptance_indx
      ~AggregatingAcceptanceModel.update_offered_indx
      ~AggregatingAcceptanceModel.update_rejected_indx

   .. rubric:: Methods Documentation

   .. automethod:: probability_of_acceptance_indx
   .. automethod:: update_offered_indx
   .. automethod:: update_rejected_indx
