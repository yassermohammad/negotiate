NegotiatorUtility
=================

.. currentmodule:: negotiate.apps.scml

.. autoclass:: NegotiatorUtility
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~NegotiatorUtility.__call__
      ~NegotiatorUtility.call
      ~NegotiatorUtility.xml

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: call
   .. automethod:: xml
