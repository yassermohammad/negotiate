MappingUtilityFunction
======================

.. currentmodule:: negotiate.utilities

.. autoclass:: MappingUtilityFunction
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~MappingUtilityFunction.__call__
      ~MappingUtilityFunction.xml

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: xml
