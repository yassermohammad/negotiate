SAOState
========

.. currentmodule:: negotiate.sao

.. autoclass:: SAOState
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~SAOState.current_offer
      ~SAOState.current_offerer
      ~SAOState.n_acceptances

   .. rubric:: Attributes Documentation

   .. autoattribute:: current_offer
   .. autoattribute:: current_offerer
   .. autoattribute:: n_acceptances
