OnlyBestNegotiator
==================

.. currentmodule:: negotiate.sao

.. autoclass:: OnlyBestNegotiator
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~OnlyBestNegotiator.eu

   .. rubric:: Methods Summary

   .. autosummary::

      ~OnlyBestNegotiator.acceptable
      ~OnlyBestNegotiator.on_negotiation_start
      ~OnlyBestNegotiator.propose_
      ~OnlyBestNegotiator.respond_

   .. rubric:: Attributes Documentation

   .. autoattribute:: eu

   .. rubric:: Methods Documentation

   .. automethod:: acceptable
   .. automethod:: on_negotiation_start
   .. automethod:: propose_
   .. automethod:: respond_
