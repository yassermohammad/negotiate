Negotiator
==========

.. currentmodule:: negotiate.negotiators

.. autoclass:: Negotiator
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Negotiator.capabilities
      ~Negotiator.reserved_value
      ~Negotiator.ufun

   .. rubric:: Methods Summary

   .. autosummary::

      ~Negotiator.add_capabilities
      ~Negotiator.before_joining
      ~Negotiator.compare
      ~Negotiator.isin
      ~Negotiator.on_leave
      ~Negotiator.on_mechanism_error
      ~Negotiator.on_negotiation_end
      ~Negotiator.on_negotiation_start
      ~Negotiator.on_notification
      ~Negotiator.on_round_end
      ~Negotiator.on_round_start

   .. rubric:: Attributes Documentation

   .. autoattribute:: capabilities
   .. autoattribute:: reserved_value
   .. autoattribute:: ufun

   .. rubric:: Methods Documentation

   .. automethod:: add_capabilities
   .. automethod:: before_joining
   .. automethod:: compare
   .. automethod:: isin
   .. automethod:: on_leave
   .. automethod:: on_mechanism_error
   .. automethod:: on_negotiation_end
   .. automethod:: on_negotiation_start
   .. automethod:: on_notification
   .. automethod:: on_round_end
   .. automethod:: on_round_start
