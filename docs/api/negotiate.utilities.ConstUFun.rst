ConstUFun
=========

.. currentmodule:: negotiate.utilities

.. autoclass:: ConstUFun
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~ConstUFun.__call__
      ~ConstUFun.xml

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: xml
