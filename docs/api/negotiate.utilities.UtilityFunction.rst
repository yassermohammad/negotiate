UtilityFunction
===============

.. currentmodule:: negotiate.utilities

.. autoclass:: UtilityFunction
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~UtilityFunction.base_type
      ~UtilityFunction.is_dynamic
      ~UtilityFunction.type

   .. rubric:: Methods Summary

   .. autosummary::

      ~UtilityFunction.__call__
      ~UtilityFunction.approximate
      ~UtilityFunction.compare
      ~UtilityFunction.conflict_level
      ~UtilityFunction.eu
      ~UtilityFunction.from_genius
      ~UtilityFunction.from_xml_str
      ~UtilityFunction.generate_bilateral
      ~UtilityFunction.generate_random_bilateral
      ~UtilityFunction.to_genius
      ~UtilityFunction.to_xml_str
      ~UtilityFunction.winwin_level
      ~UtilityFunction.xml

   .. rubric:: Attributes Documentation

   .. autoattribute:: base_type
   .. autoattribute:: is_dynamic
   .. autoattribute:: type

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: approximate
   .. automethod:: compare
   .. automethod:: conflict_level
   .. automethod:: eu
   .. automethod:: from_genius
   .. automethod:: from_xml_str
   .. automethod:: generate_bilateral
   .. automethod:: generate_random_bilateral
   .. automethod:: to_genius
   .. automethod:: to_xml_str
   .. automethod:: winwin_level
   .. automethod:: xml
