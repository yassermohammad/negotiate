ToughNegotiator
===============

.. currentmodule:: negotiate.sao

.. autoclass:: ToughNegotiator
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~ToughNegotiator.eu

   .. rubric:: Methods Summary

   .. autosummary::

      ~ToughNegotiator.on_negotiation_start
      ~ToughNegotiator.propose_
      ~ToughNegotiator.respond_

   .. rubric:: Attributes Documentation

   .. autoattribute:: eu

   .. rubric:: Methods Documentation

   .. automethod:: on_negotiation_start
   .. automethod:: propose_
   .. automethod:: respond_
