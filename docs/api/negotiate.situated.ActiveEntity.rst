ActiveEntity
============

.. currentmodule:: negotiate.situated

.. autoclass:: ActiveEntity
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~ActiveEntity.step

   .. rubric:: Methods Documentation

   .. automethod:: step
