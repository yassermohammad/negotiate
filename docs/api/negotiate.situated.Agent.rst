Agent
=====

.. currentmodule:: negotiate.situated

.. autoclass:: Agent
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~Agent.before_joining_negotiation
      ~Agent.init
      ~Agent.on_contract_cancelled
      ~Agent.on_contract_signed
      ~Agent.on_event
      ~Agent.on_neg_request_accepted
      ~Agent.on_neg_request_rejected
      ~Agent.on_negotiation_failure
      ~Agent.on_negotiation_success
      ~Agent.propose
      ~Agent.request_negotiation
      ~Agent.respond
      ~Agent.sign_contract

   .. rubric:: Methods Documentation

   .. automethod:: before_joining_negotiation
   .. automethod:: init
   .. automethod:: on_contract_cancelled
   .. automethod:: on_contract_signed
   .. automethod:: on_event
   .. automethod:: on_neg_request_accepted
   .. automethod:: on_neg_request_rejected
   .. automethod:: on_negotiation_failure
   .. automethod:: on_negotiation_success
   .. automethod:: propose
   .. automethod:: request_negotiation
   .. automethod:: respond
   .. automethod:: sign_contract
