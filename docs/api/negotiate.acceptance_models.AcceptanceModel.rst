AcceptanceModel
===============

.. currentmodule:: negotiate.acceptance_models

.. autoclass:: AcceptanceModel
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~AcceptanceModel.acceptance_probabilities
      ~AcceptanceModel.probability_of_acceptance
      ~AcceptanceModel.probability_of_acceptance_indx
      ~AcceptanceModel.update_accepted
      ~AcceptanceModel.update_offered
      ~AcceptanceModel.update_offered_indx
      ~AcceptanceModel.update_rejected
      ~AcceptanceModel.update_rejected_indx

   .. rubric:: Methods Documentation

   .. automethod:: acceptance_probabilities
   .. automethod:: probability_of_acceptance
   .. automethod:: probability_of_acceptance_indx
   .. automethod:: update_accepted
   .. automethod:: update_offered
   .. automethod:: update_offered_indx
   .. automethod:: update_rejected
   .. automethod:: update_rejected_indx
