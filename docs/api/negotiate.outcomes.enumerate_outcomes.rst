enumerate_outcomes
==================

.. currentmodule:: negotiate.outcomes

.. autofunction:: enumerate_outcomes
