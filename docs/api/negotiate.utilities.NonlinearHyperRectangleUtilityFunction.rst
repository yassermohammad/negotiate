NonlinearHyperRectangleUtilityFunction
======================================

.. currentmodule:: negotiate.utilities

.. autoclass:: NonlinearHyperRectangleUtilityFunction
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~NonlinearHyperRectangleUtilityFunction.__call__
      ~NonlinearHyperRectangleUtilityFunction.xml

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: xml
