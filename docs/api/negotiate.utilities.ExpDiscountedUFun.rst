ExpDiscountedUFun
=================

.. currentmodule:: negotiate.utilities

.. autoclass:: ExpDiscountedUFun
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~ExpDiscountedUFun.base_type
      ~ExpDiscountedUFun.type

   .. rubric:: Methods Summary

   .. autosummary::

      ~ExpDiscountedUFun.__call__
      ~ExpDiscountedUFun.xml

   .. rubric:: Attributes Documentation

   .. autoattribute:: base_type
   .. autoattribute:: type

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: xml
