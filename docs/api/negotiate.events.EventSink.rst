EventSink
=========

.. currentmodule:: negotiate.events

.. autoclass:: EventSink
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~EventSink.on_event

   .. rubric:: Methods Documentation

   .. automethod:: on_event
