register_all_mechanisms
=======================

.. currentmodule:: negotiate.common

.. autofunction:: register_all_mechanisms
