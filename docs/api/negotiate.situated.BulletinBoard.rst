BulletinBoard
=============

.. currentmodule:: negotiate.situated

.. autoclass:: BulletinBoard
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~BulletinBoard.add_section
      ~BulletinBoard.query
      ~BulletinBoard.read
      ~BulletinBoard.record
      ~BulletinBoard.remove

   .. rubric:: Methods Documentation

   .. automethod:: add_section
   .. automethod:: query
   .. automethod:: read
   .. automethod:: record
   .. automethod:: remove
