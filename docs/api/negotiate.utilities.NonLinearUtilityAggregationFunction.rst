NonLinearUtilityAggregationFunction
===================================

.. currentmodule:: negotiate.utilities

.. autoclass:: NonLinearUtilityAggregationFunction
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~NonLinearUtilityAggregationFunction.__call__
      ~NonLinearUtilityAggregationFunction.xml

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: xml
