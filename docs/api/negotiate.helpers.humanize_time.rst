humanize_time
=============

.. currentmodule:: negotiate.helpers

.. autofunction:: humanize_time
