Process
=======

.. currentmodule:: negotiate.apps.scml

.. autoclass:: Process
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Process.historical_cost
      ~Process.id
      ~Process.inputs
      ~Process.name
      ~Process.outputs
      ~Process.production_level

   .. rubric:: Attributes Documentation

   .. autoattribute:: historical_cost
   .. autoattribute:: id
   .. autoattribute:: inputs
   .. autoattribute:: name
   .. autoattribute:: outputs
   .. autoattribute:: production_level
