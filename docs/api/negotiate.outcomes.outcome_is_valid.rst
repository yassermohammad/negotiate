outcome_is_valid
================

.. currentmodule:: negotiate.outcomes

.. autofunction:: outcome_is_valid
