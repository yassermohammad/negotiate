HyperRectangleUtilityFunction
=============================

.. currentmodule:: negotiate.utilities

.. autoclass:: HyperRectangleUtilityFunction
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~HyperRectangleUtilityFunction.__call__
      ~HyperRectangleUtilityFunction.adjust_params
      ~HyperRectangleUtilityFunction.xml

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: adjust_params
   .. automethod:: xml
