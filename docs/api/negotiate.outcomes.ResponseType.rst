ResponseType
============

.. currentmodule:: negotiate.outcomes

.. autoclass:: ResponseType
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~ResponseType.ACCEPT_OFFER
      ~ResponseType.END_NEGOTIATION
      ~ResponseType.NO_RESPONSE
      ~ResponseType.REJECT_OFFER

   .. rubric:: Attributes Documentation

   .. autoattribute:: ACCEPT_OFFER
   .. autoattribute:: END_NEGOTIATION
   .. autoattribute:: NO_RESPONSE
   .. autoattribute:: REJECT_OFFER
