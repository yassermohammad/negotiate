ManufacturingProfile
====================

.. currentmodule:: negotiate.apps.scml

.. autoclass:: ManufacturingProfile
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~ManufacturingProfile.cancellation_cost
      ~ManufacturingProfile.cost
      ~ManufacturingProfile.initial_pause_cost
      ~ManufacturingProfile.n_steps
      ~ManufacturingProfile.resumption_cost
      ~ManufacturingProfile.running_pause_cost

   .. rubric:: Attributes Documentation

   .. autoattribute:: cancellation_cost
   .. autoattribute:: cost
   .. autoattribute:: initial_pause_cost
   .. autoattribute:: n_steps
   .. autoattribute:: resumption_cost
   .. autoattribute:: running_pause_cost
