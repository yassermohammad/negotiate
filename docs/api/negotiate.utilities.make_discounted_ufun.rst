make_discounted_ufun
====================

.. currentmodule:: negotiate.utilities

.. autofunction:: make_discounted_ufun
