RandomNegotiator
================

.. currentmodule:: negotiate.sao

.. autoclass:: RandomNegotiator
   :show-inheritance:
