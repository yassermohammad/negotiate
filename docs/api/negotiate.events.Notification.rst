Notification
============

.. currentmodule:: negotiate.events

.. autoclass:: Notification
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Notification.data
      ~Notification.type

   .. rubric:: Attributes Documentation

   .. autoattribute:: data
   .. autoattribute:: type
