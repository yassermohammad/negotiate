LimitedOutcomesAcceptor
=======================

.. currentmodule:: negotiate.sao

.. autoclass:: LimitedOutcomesAcceptor
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~LimitedOutcomesAcceptor.propose_
      ~LimitedOutcomesAcceptor.respond_

   .. rubric:: Methods Documentation

   .. automethod:: propose_
   .. automethod:: respond_
