SCMLWorld
=========

.. currentmodule:: negotiate.apps.scml

.. autoclass:: SCMLWorld
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~SCMLWorld.winners

   .. rubric:: Methods Summary

   .. autosummary::

      ~SCMLWorld.buy_insurance
      ~SCMLWorld.buy_loan
      ~SCMLWorld.evaluate_insurance
      ~SCMLWorld.evaluate_loan
      ~SCMLWorld.execute
      ~SCMLWorld.join
      ~SCMLWorld.on_contract_signed
      ~SCMLWorld.on_event
      ~SCMLWorld.random
      ~SCMLWorld.random_small
      ~SCMLWorld.request_negotiation
      ~SCMLWorld.run_negotiation
      ~SCMLWorld.set_consumers
      ~SCMLWorld.set_factories
      ~SCMLWorld.set_factory_managers
      ~SCMLWorld.set_miners
      ~SCMLWorld.set_processes
      ~SCMLWorld.set_products
      ~SCMLWorld.single_path_world
      ~SCMLWorld.state

   .. rubric:: Attributes Documentation

   .. autoattribute:: winners

   .. rubric:: Methods Documentation

   .. automethod:: buy_insurance
   .. automethod:: buy_loan
   .. automethod:: evaluate_insurance
   .. automethod:: evaluate_loan
   .. automethod:: execute
   .. automethod:: join
   .. automethod:: on_contract_signed
   .. automethod:: on_event
   .. automethod:: random
   .. automethod:: random_small
   .. automethod:: request_negotiation
   .. automethod:: run_negotiation
   .. automethod:: set_consumers
   .. automethod:: set_factories
   .. automethod:: set_factory_managers
   .. automethod:: set_miners
   .. automethod:: set_processes
   .. automethod:: set_products
   .. automethod:: single_path_world
   .. automethod:: state
