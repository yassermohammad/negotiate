ConfigReader
============

.. currentmodule:: negotiate.helpers

.. autoclass:: ConfigReader
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~ConfigReader.from_config
      ~ConfigReader.read_config

   .. rubric:: Methods Documentation

   .. automethod:: from_config
   .. automethod:: read_config
