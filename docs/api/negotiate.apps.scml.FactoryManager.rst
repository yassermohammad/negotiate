FactoryManager
==============

.. currentmodule:: negotiate.apps.scml

.. autoclass:: FactoryManager
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~FactoryManager.confirm_contract_execution
      ~FactoryManager.confirm_loan
      ~FactoryManager.init
      ~FactoryManager.on_breach_by_another
      ~FactoryManager.on_breach_by_self
      ~FactoryManager.on_breach_meta_negotiation
      ~FactoryManager.on_event
      ~FactoryManager.on_renegotiation_request
      ~FactoryManager.total_utility

   .. rubric:: Methods Documentation

   .. automethod:: confirm_contract_execution
   .. automethod:: confirm_loan
   .. automethod:: init
   .. automethod:: on_breach_by_another
   .. automethod:: on_breach_by_self
   .. automethod:: on_breach_meta_negotiation
   .. automethod:: on_event
   .. automethod:: on_renegotiation_request
   .. automethod:: total_utility
