SAONegotiator
=============

.. currentmodule:: negotiate.sao

.. autoclass:: SAONegotiator
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~SAONegotiator.counter
      ~SAONegotiator.on_notification
      ~SAONegotiator.on_partner_proposal
      ~SAONegotiator.on_partner_refused_to_propose
      ~SAONegotiator.on_partner_response
      ~SAONegotiator.propose
      ~SAONegotiator.propose_
      ~SAONegotiator.respond
      ~SAONegotiator.respond_

   .. rubric:: Methods Documentation

   .. automethod:: counter
   .. automethod:: on_notification
   .. automethod:: on_partner_proposal
   .. automethod:: on_partner_refused_to_propose
   .. automethod:: on_partner_response
   .. automethod:: propose
   .. automethod:: propose_
   .. automethod:: respond
   .. automethod:: respond_
