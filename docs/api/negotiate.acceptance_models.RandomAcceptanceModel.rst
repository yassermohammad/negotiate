RandomAcceptanceModel
=====================

.. currentmodule:: negotiate.acceptance_models

.. autoclass:: RandomAcceptanceModel
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~RandomAcceptanceModel.probability_of_acceptance_indx
      ~RandomAcceptanceModel.update_offered_indx
      ~RandomAcceptanceModel.update_rejected_indx

   .. rubric:: Methods Documentation

   .. automethod:: probability_of_acceptance_indx
   .. automethod:: update_offered_indx
   .. automethod:: update_rejected_indx
