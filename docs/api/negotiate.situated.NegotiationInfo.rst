NegotiationInfo
===============

.. currentmodule:: negotiate.situated

.. autoclass:: NegotiationInfo
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~NegotiationInfo.rejectors

   .. rubric:: Attributes Documentation

   .. autoattribute:: rejectors
