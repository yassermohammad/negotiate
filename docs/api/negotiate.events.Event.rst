Event
=====

.. currentmodule:: negotiate.events

.. autoclass:: Event
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Event.data
      ~Event.type

   .. rubric:: Attributes Documentation

   .. autoattribute:: data
   .. autoattribute:: type
