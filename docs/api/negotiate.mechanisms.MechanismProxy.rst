MechanismProxy
==============

.. currentmodule:: negotiate.mechanisms

.. autoclass:: MechanismProxy
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~MechanismProxy.agreement
      ~MechanismProxy.all
      ~MechanismProxy.completed
      ~MechanismProxy.dynamic_entry
      ~MechanismProxy.history
      ~MechanismProxy.issues
      ~MechanismProxy.max_n_agents
      ~MechanismProxy.n_outcomes
      ~MechanismProxy.n_steps
      ~MechanismProxy.negotiators
      ~MechanismProxy.outcomes
      ~MechanismProxy.participants
      ~MechanismProxy.relative_time
      ~MechanismProxy.remaining_steps
      ~MechanismProxy.remaining_time
      ~MechanismProxy.requirements
      ~MechanismProxy.running
      ~MechanismProxy.state
      ~MechanismProxy.time
      ~MechanismProxy.time_limit

   .. rubric:: Methods Summary

   .. autosummary::

      ~MechanismProxy.add
      ~MechanismProxy.add_requirements
      ~MechanismProxy.can_accept_more_agents
      ~MechanismProxy.can_enter
      ~MechanismProxy.can_leave
      ~MechanismProxy.can_participate
      ~MechanismProxy.discrete_outcomes
      ~MechanismProxy.extra_state
      ~MechanismProxy.get_info
      ~MechanismProxy.is_satisfying
      ~MechanismProxy.is_valid
      ~MechanismProxy.on_mechanism_error
      ~MechanismProxy.on_negotiation_end
      ~MechanismProxy.on_negotiation_start
      ~MechanismProxy.pareto_frontier
      ~MechanismProxy.plot
      ~MechanismProxy.random_outcomes
      ~MechanismProxy.remove
      ~MechanismProxy.remove_requirements
      ~MechanismProxy.round
      ~MechanismProxy.run
      ~MechanismProxy.step

   .. rubric:: Attributes Documentation

   .. autoattribute:: agreement
   .. autoattribute:: all
   .. autoattribute:: completed
   .. autoattribute:: dynamic_entry
   .. autoattribute:: history
   .. autoattribute:: issues
   .. autoattribute:: max_n_agents
   .. autoattribute:: n_outcomes
   .. autoattribute:: n_steps
   .. autoattribute:: negotiators
   .. autoattribute:: outcomes
   .. autoattribute:: participants
   .. autoattribute:: relative_time
   .. autoattribute:: remaining_steps
   .. autoattribute:: remaining_time
   .. autoattribute:: requirements
   .. autoattribute:: running
   .. autoattribute:: state
   .. autoattribute:: time
   .. autoattribute:: time_limit

   .. rubric:: Methods Documentation

   .. automethod:: add
   .. automethod:: add_requirements
   .. automethod:: can_accept_more_agents
   .. automethod:: can_enter
   .. automethod:: can_leave
   .. automethod:: can_participate
   .. automethod:: discrete_outcomes
   .. automethod:: extra_state
   .. automethod:: get_info
   .. automethod:: is_satisfying
   .. automethod:: is_valid
   .. automethod:: on_mechanism_error
   .. automethod:: on_negotiation_end
   .. automethod:: on_negotiation_start
   .. automethod:: pareto_frontier
   .. automethod:: plot
   .. automethod:: random_outcomes
   .. automethod:: remove
   .. automethod:: remove_requirements
   .. automethod:: round
   .. automethod:: run
   .. automethod:: step
