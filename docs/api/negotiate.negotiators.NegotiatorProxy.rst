NegotiatorProxy
===============

.. currentmodule:: negotiate.negotiators

.. autoclass:: NegotiatorProxy
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~NegotiatorProxy.capabilities
      ~NegotiatorProxy.reserved_value
      ~NegotiatorProxy.ufun

   .. rubric:: Methods Summary

   .. autosummary::

      ~NegotiatorProxy.add_capabilities
      ~NegotiatorProxy.before_joining
      ~NegotiatorProxy.compare
      ~NegotiatorProxy.isin
      ~NegotiatorProxy.on_leave
      ~NegotiatorProxy.on_mechanism_error
      ~NegotiatorProxy.on_negotiation_end
      ~NegotiatorProxy.on_negotiation_start
      ~NegotiatorProxy.on_notification
      ~NegotiatorProxy.on_round_end
      ~NegotiatorProxy.on_round_start

   .. rubric:: Attributes Documentation

   .. autoattribute:: capabilities
   .. autoattribute:: reserved_value
   .. autoattribute:: ufun

   .. rubric:: Methods Documentation

   .. automethod:: add_capabilities
   .. automethod:: before_joining
   .. automethod:: compare
   .. automethod:: isin
   .. automethod:: on_leave
   .. automethod:: on_mechanism_error
   .. automethod:: on_negotiation_end
   .. automethod:: on_negotiation_start
   .. automethod:: on_notification
   .. automethod:: on_round_end
   .. automethod:: on_round_start
