Notifiable
==========

.. currentmodule:: negotiate.events

.. autoclass:: Notifiable
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~Notifiable.on_notification

   .. rubric:: Methods Documentation

   .. automethod:: on_notification
