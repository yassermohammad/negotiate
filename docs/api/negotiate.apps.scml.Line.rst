Line
====

.. currentmodule:: negotiate.apps.scml

.. autoclass:: Line
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Line.n_steps
      ~Line.schedule
      ~Line.state

   .. rubric:: Methods Summary

   .. autosummary::

      ~Line.copy
      ~Line.init_schedule
      ~Line.schedule_job
      ~Line.step

   .. rubric:: Attributes Documentation

   .. autoattribute:: n_steps
   .. autoattribute:: schedule
   .. autoattribute:: state

   .. rubric:: Methods Documentation

   .. automethod:: copy
   .. automethod:: init_schedule
   .. automethod:: schedule_job
   .. automethod:: step
