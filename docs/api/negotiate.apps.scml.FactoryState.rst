FactoryState
============

.. currentmodule:: negotiate.apps.scml

.. autoclass:: FactoryState
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~FactoryState.balance
      ~FactoryState.loans
      ~FactoryState.wallet

   .. rubric:: Methods Summary

   .. autosummary::

      ~FactoryState.copy

   .. rubric:: Attributes Documentation

   .. autoattribute:: balance
   .. autoattribute:: loans
   .. autoattribute:: wallet

   .. rubric:: Methods Documentation

   .. automethod:: copy
