Contract
========

.. currentmodule:: negotiate.situated

.. autoclass:: Contract
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Contract.agreement
      ~Contract.concluded_at
      ~Contract.mechanism_state
      ~Contract.signed_at
      ~Contract.to_be_signed_at

   .. rubric:: Attributes Documentation

   .. autoattribute:: agreement
   .. autoattribute:: concluded_at
   .. autoattribute:: mechanism_state
   .. autoattribute:: signed_at
   .. autoattribute:: to_be_signed_at
