is_nonzero_file
===============

.. currentmodule:: negotiate.helpers

.. autofunction:: is_nonzero_file
