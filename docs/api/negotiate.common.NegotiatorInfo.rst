NegotiatorInfo
==============

.. currentmodule:: negotiate.common

.. autoclass:: NegotiatorInfo
   :show-inheritance:
