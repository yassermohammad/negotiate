MechanismRoundResult
====================

.. currentmodule:: negotiate.mechanisms

.. autoclass:: MechanismRoundResult
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~MechanismRoundResult.agreement
      ~MechanismRoundResult.broken
      ~MechanismRoundResult.error
      ~MechanismRoundResult.error_details
      ~MechanismRoundResult.timedout

   .. rubric:: Attributes Documentation

   .. autoattribute:: agreement
   .. autoattribute:: broken
   .. autoattribute:: error
   .. autoattribute:: error_details
   .. autoattribute:: timedout
