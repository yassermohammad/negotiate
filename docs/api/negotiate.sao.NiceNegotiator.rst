NiceNegotiator
==============

.. currentmodule:: negotiate.sao

.. autoclass:: NiceNegotiator
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~NiceNegotiator.propose_
      ~NiceNegotiator.respond_

   .. rubric:: Methods Documentation

   .. automethod:: propose_
   .. automethod:: respond_
