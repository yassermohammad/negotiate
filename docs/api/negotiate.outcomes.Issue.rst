Issue
=====

.. currentmodule:: negotiate.outcomes

.. autoclass:: Issue
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Issue.all
      ~Issue.outcome_range
      ~Issue.type

   .. rubric:: Methods Summary

   .. autosummary::

      ~Issue.alli
      ~Issue.cardinality
      ~Issue.enumerate
      ~Issue.from_genius
      ~Issue.from_xml_str
      ~Issue.generate
      ~Issue.is_continuous
      ~Issue.is_discrete
      ~Issue.n_outcomes
      ~Issue.rand
      ~Issue.rand_invalid
      ~Issue.rand_outcomes
      ~Issue.rand_valid
      ~Issue.sample
      ~Issue.to_genius
      ~Issue.to_xml_str

   .. rubric:: Attributes Documentation

   .. autoattribute:: all
   .. autoattribute:: outcome_range
   .. autoattribute:: type

   .. rubric:: Methods Documentation

   .. automethod:: alli
   .. automethod:: cardinality
   .. automethod:: enumerate
   .. automethod:: from_genius
   .. automethod:: from_xml_str
   .. automethod:: generate
   .. automethod:: is_continuous
   .. automethod:: is_discrete
   .. automethod:: n_outcomes
   .. automethod:: rand
   .. automethod:: rand_invalid
   .. automethod:: rand_outcomes
   .. automethod:: rand_valid
   .. automethod:: sample
   .. automethod:: to_genius
   .. automethod:: to_xml_str
