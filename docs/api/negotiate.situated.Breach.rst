Breach
======

.. currentmodule:: negotiate.situated

.. autoclass:: Breach
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Breach.level
      ~Breach.step

   .. rubric:: Attributes Documentation

   .. autoattribute:: level
   .. autoattribute:: step
