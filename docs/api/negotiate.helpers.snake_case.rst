snake_case
==========

.. currentmodule:: negotiate.helpers

.. autofunction:: snake_case
