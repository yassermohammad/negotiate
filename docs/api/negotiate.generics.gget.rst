gget
====

.. currentmodule:: negotiate.generics

.. autofunction:: gget
