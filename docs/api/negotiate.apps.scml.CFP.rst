CFP
===

.. currentmodule:: negotiate.apps.scml

.. autoclass:: CFP
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~CFP.issues
      ~CFP.max_quantity
      ~CFP.max_time
      ~CFP.max_unit_price
      ~CFP.min_quantity
      ~CFP.min_time
      ~CFP.min_unit_price
      ~CFP.money_resolution
      ~CFP.outcomes
      ~CFP.penalty
      ~CFP.signing_delay

   .. rubric:: Methods Summary

   .. autosummary::

      ~CFP.satisfies

   .. rubric:: Attributes Documentation

   .. autoattribute:: issues
   .. autoattribute:: max_quantity
   .. autoattribute:: max_time
   .. autoattribute:: max_unit_price
   .. autoattribute:: min_quantity
   .. autoattribute:: min_time
   .. autoattribute:: min_unit_price
   .. autoattribute:: money_resolution
   .. autoattribute:: outcomes
   .. autoattribute:: penalty
   .. autoattribute:: signing_delay

   .. rubric:: Methods Documentation

   .. automethod:: satisfies
