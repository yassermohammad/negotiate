BreachProcessing
================

.. currentmodule:: negotiate.situated

.. autoclass:: BreachProcessing
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~BreachProcessing.META_NEGOTIATION
      ~BreachProcessing.NONE
      ~BreachProcessing.VICTIM_THEN_PERPETRATOR

   .. rubric:: Attributes Documentation

   .. autoattribute:: META_NEGOTIATION
   .. autoattribute:: NONE
   .. autoattribute:: VICTIM_THEN_PERPETRATOR
