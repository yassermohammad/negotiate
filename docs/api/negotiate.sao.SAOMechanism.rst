SAOMechanism
============

.. currentmodule:: negotiate.sao

.. autoclass:: SAOMechanism
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~SAOMechanism.extra_state
      ~SAOMechanism.on_negotiation_start
      ~SAOMechanism.round

   .. rubric:: Methods Documentation

   .. automethod:: extra_state
   .. automethod:: on_negotiation_start
   .. automethod:: round
