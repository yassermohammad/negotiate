AgentWorldInterface
===================

.. currentmodule:: negotiate.situated

.. autoclass:: AgentWorldInterface
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~AgentWorldInterface.bulletin_board
      ~AgentWorldInterface.current_step
      ~AgentWorldInterface.default_signing_delay
      ~AgentWorldInterface.n_steps
      ~AgentWorldInterface.relative_time
      ~AgentWorldInterface.state

   .. rubric:: Methods Summary

   .. autosummary::

      ~AgentWorldInterface.execute
      ~AgentWorldInterface.logdebug
      ~AgentWorldInterface.logerror
      ~AgentWorldInterface.loginfo
      ~AgentWorldInterface.logwarning
      ~AgentWorldInterface.request_negotiation

   .. rubric:: Attributes Documentation

   .. autoattribute:: bulletin_board
   .. autoattribute:: current_step
   .. autoattribute:: default_signing_delay
   .. autoattribute:: n_steps
   .. autoattribute:: relative_time
   .. autoattribute:: state

   .. rubric:: Methods Documentation

   .. automethod:: execute
   .. automethod:: logdebug
   .. automethod:: logerror
   .. automethod:: loginfo
   .. automethod:: logwarning
   .. automethod:: request_negotiation
