pareto_frontier
===============

.. currentmodule:: negotiate.utilities

.. autofunction:: pareto_frontier
