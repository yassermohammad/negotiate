AspirationMixin
===============

.. currentmodule:: negotiate.negotiators

.. autoclass:: AspirationMixin
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~AspirationMixin.aspiration
      ~AspirationMixin.aspiration_init

   .. rubric:: Methods Documentation

   .. automethod:: aspiration
   .. automethod:: aspiration_init
