AdaptiveAcceptanceModel
=======================

.. currentmodule:: negotiate.acceptance_models

.. autoclass:: AdaptiveAcceptanceModel
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~AdaptiveAcceptanceModel.acceptance_probabilities
      ~AdaptiveAcceptanceModel.from_negotiation
      ~AdaptiveAcceptanceModel.probability_of_acceptance_indx
      ~AdaptiveAcceptanceModel.update_offered_indx
      ~AdaptiveAcceptanceModel.update_rejected_indx

   .. rubric:: Methods Documentation

   .. automethod:: acceptance_probabilities
   .. automethod:: from_negotiation
   .. automethod:: probability_of_acceptance_indx
   .. automethod:: update_offered_indx
   .. automethod:: update_rejected_indx
