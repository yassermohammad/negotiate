Factory
=======

.. currentmodule:: negotiate.apps.scml

.. autoclass:: Factory
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Factory.manager
      ~Factory.max_storage
      ~Factory.n_processes
      ~Factory.n_products
      ~Factory.n_scheduling_steps
      ~Factory.n_steps
      ~Factory.predicted_balance
      ~Factory.predicted_reserved_storage
      ~Factory.predicted_storage
      ~Factory.predicted_total_storage
      ~Factory.schedule
      ~Factory.state

   .. rubric:: Methods Summary

   .. autosummary::

      ~Factory.copy
      ~Factory.find_needs
      ~Factory.get_line
      ~Factory.init_schedule
      ~Factory.schedule_job
      ~Factory.step
      ~Factory.update_fields

   .. rubric:: Attributes Documentation

   .. autoattribute:: manager
   .. autoattribute:: max_storage
   .. autoattribute:: n_processes
   .. autoattribute:: n_products
   .. autoattribute:: n_scheduling_steps
   .. autoattribute:: n_steps
   .. autoattribute:: predicted_balance
   .. autoattribute:: predicted_reserved_storage
   .. autoattribute:: predicted_storage
   .. autoattribute:: predicted_total_storage
   .. autoattribute:: schedule
   .. autoattribute:: state

   .. rubric:: Methods Documentation

   .. automethod:: copy
   .. automethod:: find_needs
   .. automethod:: get_line
   .. automethod:: init_schedule
   .. automethod:: schedule_job
   .. automethod:: step
   .. automethod:: update_fields
