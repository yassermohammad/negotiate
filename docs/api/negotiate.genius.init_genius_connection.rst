init_genius_connection
======================

.. currentmodule:: negotiate.genius

.. autofunction:: init_genius_connection
