EventSource
===========

.. currentmodule:: negotiate.events

.. autoclass:: EventSource
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~EventSource.announce
      ~EventSource.register_listener

   .. rubric:: Methods Documentation

   .. automethod:: announce
   .. automethod:: register_listener
