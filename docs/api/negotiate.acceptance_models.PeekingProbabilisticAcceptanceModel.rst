PeekingProbabilisticAcceptanceModel
===================================

.. currentmodule:: negotiate.acceptance_models

.. autoclass:: PeekingProbabilisticAcceptanceModel
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~PeekingProbabilisticAcceptanceModel.probability_of_acceptance_indx
      ~PeekingProbabilisticAcceptanceModel.update_offered_indx
      ~PeekingProbabilisticAcceptanceModel.update_rejected_indx

   .. rubric:: Methods Documentation

   .. automethod:: probability_of_acceptance_indx
   .. automethod:: update_offered_indx
   .. automethod:: update_rejected_indx
