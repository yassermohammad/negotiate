ReturnCause
===========

.. currentmodule:: negotiate.helpers

.. autoclass:: ReturnCause
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~ReturnCause.FAILURE
      ~ReturnCause.SUCCESS
      ~ReturnCause.TIMEOUT

   .. rubric:: Attributes Documentation

   .. autoattribute:: FAILURE
   .. autoattribute:: SUCCESS
   .. autoattribute:: TIMEOUT
