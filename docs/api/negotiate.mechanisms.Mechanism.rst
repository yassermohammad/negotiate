Mechanism
=========

.. currentmodule:: negotiate.mechanisms

.. autoclass:: Mechanism
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Mechanism.agreement
      ~Mechanism.all
      ~Mechanism.completed
      ~Mechanism.dynamic_entry
      ~Mechanism.history
      ~Mechanism.issues
      ~Mechanism.max_n_agents
      ~Mechanism.n_outcomes
      ~Mechanism.n_steps
      ~Mechanism.negotiators
      ~Mechanism.outcomes
      ~Mechanism.participants
      ~Mechanism.relative_time
      ~Mechanism.remaining_steps
      ~Mechanism.remaining_time
      ~Mechanism.requirements
      ~Mechanism.running
      ~Mechanism.state
      ~Mechanism.time
      ~Mechanism.time_limit

   .. rubric:: Methods Summary

   .. autosummary::

      ~Mechanism.add
      ~Mechanism.add_requirements
      ~Mechanism.can_accept_more_agents
      ~Mechanism.can_enter
      ~Mechanism.can_leave
      ~Mechanism.can_participate
      ~Mechanism.discrete_outcomes
      ~Mechanism.extra_state
      ~Mechanism.get_info
      ~Mechanism.is_satisfying
      ~Mechanism.is_valid
      ~Mechanism.on_mechanism_error
      ~Mechanism.on_negotiation_end
      ~Mechanism.on_negotiation_start
      ~Mechanism.pareto_frontier
      ~Mechanism.plot
      ~Mechanism.random_outcomes
      ~Mechanism.remove
      ~Mechanism.remove_requirements
      ~Mechanism.round
      ~Mechanism.run
      ~Mechanism.step

   .. rubric:: Attributes Documentation

   .. autoattribute:: agreement
   .. autoattribute:: all
   .. autoattribute:: completed
   .. autoattribute:: dynamic_entry
   .. autoattribute:: history
   .. autoattribute:: issues
   .. autoattribute:: max_n_agents
   .. autoattribute:: n_outcomes
   .. autoattribute:: n_steps
   .. autoattribute:: negotiators
   .. autoattribute:: outcomes
   .. autoattribute:: participants
   .. autoattribute:: relative_time
   .. autoattribute:: remaining_steps
   .. autoattribute:: remaining_time
   .. autoattribute:: requirements
   .. autoattribute:: running
   .. autoattribute:: state
   .. autoattribute:: time
   .. autoattribute:: time_limit

   .. rubric:: Methods Documentation

   .. automethod:: add
   .. automethod:: add_requirements
   .. automethod:: can_accept_more_agents
   .. automethod:: can_enter
   .. automethod:: can_leave
   .. automethod:: can_participate
   .. automethod:: discrete_outcomes
   .. automethod:: extra_state
   .. automethod:: get_info
   .. automethod:: is_satisfying
   .. automethod:: is_valid
   .. automethod:: on_mechanism_error
   .. automethod:: on_negotiation_end
   .. automethod:: on_negotiation_start
   .. automethod:: pareto_frontier
   .. automethod:: plot
   .. automethod:: random_outcomes
   .. automethod:: remove
   .. automethod:: remove_requirements
   .. automethod:: round
   .. automethod:: run
   .. automethod:: step
