OutcomeType
===========

.. currentmodule:: negotiate.outcomes

.. autoclass:: OutcomeType
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~OutcomeType.asdict
      ~OutcomeType.astuple
      ~OutcomeType.get
      ~OutcomeType.keys
      ~OutcomeType.values

   .. rubric:: Methods Documentation

   .. automethod:: asdict
   .. automethod:: astuple
   .. automethod:: get
   .. automethod:: keys
   .. automethod:: values
