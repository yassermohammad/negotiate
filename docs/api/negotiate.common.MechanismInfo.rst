MechanismInfo
=============

.. currentmodule:: negotiate.common

.. autoclass:: MechanismInfo
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~MechanismInfo.n_negotiators
      ~MechanismInfo.participants
      ~MechanismInfo.requirements
      ~MechanismInfo.state

   .. rubric:: Methods Summary

   .. autosummary::

      ~MechanismInfo.asdict
      ~MechanismInfo.discrete_outcomes
      ~MechanismInfo.keys
      ~MechanismInfo.outcome_index
      ~MechanismInfo.random_outcomes
      ~MechanismInfo.values

   .. rubric:: Attributes Documentation

   .. autoattribute:: n_negotiators
   .. autoattribute:: participants
   .. autoattribute:: requirements
   .. autoattribute:: state

   .. rubric:: Methods Documentation

   .. automethod:: asdict
   .. automethod:: discrete_outcomes
   .. automethod:: keys
   .. automethod:: outcome_index
   .. automethod:: random_outcomes
   .. automethod:: values
