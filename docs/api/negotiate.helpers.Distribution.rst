Distribution
============

.. currentmodule:: negotiate.helpers

.. autoclass:: Distribution
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Distribution.loc
      ~Distribution.scale

   .. rubric:: Methods Summary

   .. autosummary::

      ~Distribution.around
      ~Distribution.mean
      ~Distribution.prob
      ~Distribution.sample

   .. rubric:: Attributes Documentation

   .. autoattribute:: loc
   .. autoattribute:: scale

   .. rubric:: Methods Documentation

   .. automethod:: around
   .. automethod:: mean
   .. automethod:: prob
   .. automethod:: sample
