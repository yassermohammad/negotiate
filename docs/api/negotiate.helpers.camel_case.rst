camel_case
==========

.. currentmodule:: negotiate.helpers

.. autofunction:: camel_case
