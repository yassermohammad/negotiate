unique_name
===========

.. currentmodule:: negotiate.helpers

.. autofunction:: unique_name
