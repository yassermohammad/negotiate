GeniusNegotiator
================

.. currentmodule:: negotiate.genius

.. autoclass:: GeniusNegotiator
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~GeniusNegotiator.java_name

   .. rubric:: Methods Summary

   .. autosummary::

      ~GeniusNegotiator.negotiators
      ~GeniusNegotiator.on_negotiation_start
      ~GeniusNegotiator.on_partner_proposal
      ~GeniusNegotiator.on_partner_response
      ~GeniusNegotiator.parse
      ~GeniusNegotiator.propose_
      ~GeniusNegotiator.random_negotiator
      ~GeniusNegotiator.respond_
      ~GeniusNegotiator.test

   .. rubric:: Attributes Documentation

   .. autoattribute:: java_name

   .. rubric:: Methods Documentation

   .. automethod:: negotiators
   .. automethod:: on_negotiation_start
   .. automethod:: on_partner_proposal
   .. automethod:: on_partner_response
   .. automethod:: parse
   .. automethod:: propose_
   .. automethod:: random_negotiator
   .. automethod:: respond_
   .. automethod:: test
