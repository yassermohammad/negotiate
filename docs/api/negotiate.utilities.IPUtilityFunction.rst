IPUtilityFunction
=================

.. currentmodule:: negotiate.utilities

.. autoclass:: IPUtilityFunction
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~IPUtilityFunction.__call__
      ~IPUtilityFunction.distribution
      ~IPUtilityFunction.from_mapping
      ~IPUtilityFunction.from_ufun
      ~IPUtilityFunction.key
      ~IPUtilityFunction.sample
      ~IPUtilityFunction.xml

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: distribution
   .. automethod:: from_mapping
   .. automethod:: from_ufun
   .. automethod:: key
   .. automethod:: sample
   .. automethod:: xml
