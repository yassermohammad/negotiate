SimpleTitForTatNegotiator
=========================

.. currentmodule:: negotiate.sao

.. autoclass:: SimpleTitForTatNegotiator
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~SimpleTitForTatNegotiator.on_negotiation_start
      ~SimpleTitForTatNegotiator.propose_
      ~SimpleTitForTatNegotiator.respond_

   .. rubric:: Methods Documentation

   .. automethod:: on_negotiation_start
   .. automethod:: propose_
   .. automethod:: respond_
