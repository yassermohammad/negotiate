GreedyFactoryManager
====================

.. currentmodule:: negotiate.apps.scml

.. autoclass:: GreedyFactoryManager
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~GreedyFactoryManager.can_produce
      ~GreedyFactoryManager.init
      ~GreedyFactoryManager.needs_securable
      ~GreedyFactoryManager.on_contract_signed
      ~GreedyFactoryManager.on_negotiation_failure
      ~GreedyFactoryManager.on_negotiation_request
      ~GreedyFactoryManager.on_negotiation_success
      ~GreedyFactoryManager.on_new_cfp
      ~GreedyFactoryManager.sign_contract
      ~GreedyFactoryManager.step

   .. rubric:: Methods Documentation

   .. automethod:: can_produce
   .. automethod:: init
   .. automethod:: needs_securable
   .. automethod:: on_contract_signed
   .. automethod:: on_negotiation_failure
   .. automethod:: on_negotiation_request
   .. automethod:: on_negotiation_success
   .. automethod:: on_new_cfp
   .. automethod:: sign_contract
   .. automethod:: step
