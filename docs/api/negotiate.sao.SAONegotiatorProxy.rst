SAONegotiatorProxy
==================

.. currentmodule:: negotiate.sao

.. autoclass:: SAONegotiatorProxy
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~SAONegotiatorProxy.counter
      ~SAONegotiatorProxy.on_notification
      ~SAONegotiatorProxy.on_partner_proposal
      ~SAONegotiatorProxy.on_partner_refused_to_propose
      ~SAONegotiatorProxy.on_partner_response
      ~SAONegotiatorProxy.propose
      ~SAONegotiatorProxy.propose_
      ~SAONegotiatorProxy.respond
      ~SAONegotiatorProxy.respond_

   .. rubric:: Methods Documentation

   .. automethod:: counter
   .. automethod:: on_notification
   .. automethod:: on_partner_proposal
   .. automethod:: on_partner_refused_to_propose
   .. automethod:: on_partner_response
   .. automethod:: propose
   .. automethod:: propose_
   .. automethod:: respond
   .. automethod:: respond_
