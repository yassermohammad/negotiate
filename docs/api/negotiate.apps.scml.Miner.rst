Miner
=====

.. currentmodule:: negotiate.apps.scml

.. autoclass:: Miner
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~Miner.confirm_contract_execution
      ~Miner.confirm_loan
      ~Miner.init
      ~Miner.on_breach_by_another
      ~Miner.on_breach_by_self
      ~Miner.on_breach_meta_negotiation
      ~Miner.on_negotiation_failure
      ~Miner.on_negotiation_request
      ~Miner.on_new_cfp
      ~Miner.on_renegotiation_request
      ~Miner.set_profiles
      ~Miner.step

   .. rubric:: Methods Documentation

   .. automethod:: confirm_contract_execution
   .. automethod:: confirm_loan
   .. automethod:: init
   .. automethod:: on_breach_by_another
   .. automethod:: on_breach_by_self
   .. automethod:: on_breach_meta_negotiation
   .. automethod:: on_negotiation_failure
   .. automethod:: on_negotiation_request
   .. automethod:: on_new_cfp
   .. automethod:: on_renegotiation_request
   .. automethod:: set_profiles
   .. automethod:: step
