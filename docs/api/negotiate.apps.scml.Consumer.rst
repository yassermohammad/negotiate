Consumer
========

.. currentmodule:: negotiate.apps.scml

.. autoclass:: Consumer
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~Consumer.MAX_UNIT_PRICE
      ~Consumer.RELATIVE_MAX_PRICE

   .. rubric:: Methods Summary

   .. autosummary::

      ~Consumer.confirm_contract_execution
      ~Consumer.confirm_loan
      ~Consumer.init
      ~Consumer.on_breach_by_another
      ~Consumer.on_breach_by_self
      ~Consumer.on_breach_meta_negotiation
      ~Consumer.on_contract_signed
      ~Consumer.on_negotiation_request
      ~Consumer.on_new_cfp
      ~Consumer.on_renegotiation_request
      ~Consumer.register_product_cfps
      ~Consumer.set_profiles
      ~Consumer.sign_contract
      ~Consumer.step

   .. rubric:: Attributes Documentation

   .. autoattribute:: MAX_UNIT_PRICE
   .. autoattribute:: RELATIVE_MAX_PRICE

   .. rubric:: Methods Documentation

   .. automethod:: confirm_contract_execution
   .. automethod:: confirm_loan
   .. automethod:: init
   .. automethod:: on_breach_by_another
   .. automethod:: on_breach_by_self
   .. automethod:: on_breach_meta_negotiation
   .. automethod:: on_contract_signed
   .. automethod:: on_negotiation_request
   .. automethod:: on_new_cfp
   .. automethod:: on_renegotiation_request
   .. automethod:: register_product_cfps
   .. automethod:: set_profiles
   .. automethod:: sign_contract
   .. automethod:: step
