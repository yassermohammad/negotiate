SCMLAction
==========

.. currentmodule:: negotiate.apps.scml

.. autoclass:: SCMLAction
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~SCMLAction.time

   .. rubric:: Attributes Documentation

   .. autoattribute:: time
