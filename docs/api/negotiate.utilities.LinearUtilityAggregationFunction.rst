LinearUtilityAggregationFunction
================================

.. currentmodule:: negotiate.utilities

.. autoclass:: LinearUtilityAggregationFunction
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~LinearUtilityAggregationFunction.__call__
      ~LinearUtilityAggregationFunction.xml

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: xml
