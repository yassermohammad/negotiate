Notifier
========

.. currentmodule:: negotiate.events

.. autoclass:: Notifier
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~Notifier.notify

   .. rubric:: Methods Documentation

   .. automethod:: notify
