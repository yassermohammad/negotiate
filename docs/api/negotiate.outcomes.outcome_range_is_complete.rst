outcome_range_is_complete
=========================

.. currentmodule:: negotiate.outcomes

.. autofunction:: outcome_range_is_complete
