LinDiscountedUFun
=================

.. currentmodule:: negotiate.utilities

.. autoclass:: LinDiscountedUFun
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~LinDiscountedUFun.base_type
      ~LinDiscountedUFun.type

   .. rubric:: Methods Summary

   .. autosummary::

      ~LinDiscountedUFun.__call__
      ~LinDiscountedUFun.xml

   .. rubric:: Attributes Documentation

   .. autoattribute:: base_type
   .. autoattribute:: type

   .. rubric:: Methods Documentation

   .. automethod:: __call__
   .. automethod:: xml
