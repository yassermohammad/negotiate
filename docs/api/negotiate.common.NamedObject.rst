NamedObject
===========

.. currentmodule:: negotiate.common

.. autoclass:: NamedObject
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~NamedObject.id
      ~NamedObject.name
      ~NamedObject.uuid

   .. rubric:: Methods Summary

   .. autosummary::

      ~NamedObject.create

   .. rubric:: Attributes Documentation

   .. autoattribute:: id
   .. autoattribute:: name
   .. autoattribute:: uuid

   .. rubric:: Methods Documentation

   .. automethod:: create
