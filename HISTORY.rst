=======
History
=======

0.1.0 (2018-03-05)
------------------

* Initial conception.

0.1.1 (2019-01-31)
------------------
* Situated negotiations were added
* SCM World for the SCML League of ANAC 2019 (http://web.tuat.ac.jp/~katfuji/ANAC2019/#scm)
* Elicitation was removed
