#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=6.0', ]

setup_requirements = ['pytest-runner', ]

test_requirements = ['pytest', ]

setup(
    author="Yasser Mohammad",
    author_email='yasserfarouk@gmail.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description=readme,
    entry_points={
        'console_scripts': [
            'rungenius=negotiate.scripts.rungenius.cli',
            'scml=negotiate.scripts.scml.cli',
        ],
    },
    install_requires=['numpy', 'pandas', 'scipy', 'joblib', 'pytest-runner', 'colorlog', 'py4j', 'dataclasses'
        , 'inflect', 'stringcase', 'pyyaml', 'tabulate', 'progressbar2', 'click'],
    license="Apache license",
    long_description=readme + '\n' + history,
    include_package_data=True,
    keywords='negotiate',
    name='negotiate',
    packages=find_packages(include=['negotiate']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/yasserfarouk/negotiate',
    version='0.1.1',
    zip_safe=False,
)
